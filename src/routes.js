const List = () => import(/* webpackChunkName: "list" */ './components/List/List.vue')
const Sketch = () => import(/* webpackChunkName: "sketch" */ './components/Sketch/Sketch.vue')

const routes = [
    { path: '/', component: List, name: 'lobby' },
    { path: '/:id', component: Sketch, name: 'room' },
];

export default routes;