// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuetify from 'vuetify'
import VeeValidate from 'vee-validate'
import VueSocketIO from 'vue-socket.io'
import 'vuetify/dist/vuetify.min.css'

import App from './App.vue'

import routes from './routes'

/**
 * Production
 */

const socketUrl = window.location.hostname

/**
 * Dev
 */

//const socketUrl = 'localhost:8000'

/* eslint-disable no-new */
Vue.use(new VueSocketIO({
    connection: socketUrl,
}))
Vue.use(VueRouter)
Vue.use(VeeValidate)
Vue.use(Vuetify)

const router = new VueRouter({
    mode: 'history',
    routes
})

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')
